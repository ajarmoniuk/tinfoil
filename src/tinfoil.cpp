//============================================================================
// Name        : tinfoil.cpp
// Author      : Andrzej Jarmoniuk
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#define VERSION "1.0"

#include <iostream>
#include <boost/program_options.hpp>
//#include <boost/regex.hpp>

#include "configuration.h"
#include "configurationhelper.h"
#include "daemon.h"
#include "errors.h"

using namespace std;
namespace po = boost::program_options;

//using namespace boost;

using namespace tinfoil;

int main(int argc, char **argv) {
	po::options_description desc("Allowed options");
	desc.add_options()
			("help", "display this help message")
			("version", "display version information")
			("port,p", po::value<string>(), "allows to specify listener port 'arg'")
			("verbose,v", "turns on verbose informatin")
			("config,c", po::value<string>()->default_value("config.cfg"), "allows to specify config file 'arg'");

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
		cout << desc << endl;
		return 0;
	}
	if (vm.count("version")) {
		cout << VERSION << endl;
		return 0;
	}

	error_page::init();

	configuration config;
    configuration_parser::load_from_file(config, vm["config"].as<string>());
	if (vm.count("verbose")) {
		config.set_verbose(true);
	}
	if (vm.count("port")) {
		config.set_listener_port(vm["port"].as<string>());
	}

    tinfoil::daemon d(config);
    d.start();

    return 0;
}
