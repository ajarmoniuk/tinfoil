/*
 * daemon.h
 *
 *  Created on: 22-04-2012
 *      Author: ajarmoniuk
 */
#include "configuration.h"

#ifndef DAEMON_H_
#define DAEMON_H_

namespace tinfoil {

class daemon {
private:
	configuration config;
public:
	daemon(configuration & config);
	daemon(const string & config_file = "config.cfg");
	virtual ~daemon();

	/**
	 * Starts the service and begins listening on the port defined in configuration.
	 */
	void start();
};

}

#endif /* DAEMON_H_ */
