/*
 * configuration.cpp
 *
 *  Created on: 21-04-2012
 *      Author: ajarmoniuk
 */

#include "configuration.h"

namespace tinfoil {

configuration::configuration() :
		verbose(false),
		log_file_name("/tmp/tinfoil.log"),
		loglevel(info),
		owner(""),
		owner_group(""),
		listener_addr("localhost"),
		listener_port("8080"),
		default_content("default.html")
		{
	// TODO Auto-generated constructor stub

}

configuration::~configuration() {
	// TODO Auto-generated destructor stub
}

}
