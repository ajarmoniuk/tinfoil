#ifndef _GARBAGE_THREADS_H_
#define _GARBAGE_THREADS_H_

#include <boost/thread.hpp>
#include "logger.h"

namespace tinfoil {

template<class T>
class garbage_threads {
public:
	garbage_threads() {

	}

	virtual ~garbage_threads() {

	}

	void run(T *obj) {
		try {
			boost::thread thread(&garbage_threads::run_thread, this, obj);
		} catch (runtime_error const & e) {
			throw e;
		} catch (...) {
			logger(error) << "Unknown error: " << strerror(errno);
		}
	}
private:
	void run_thread(T *obj) {
		obj->start();
		obj->join();
		try {
			delete obj;
		} catch (...) {
		}
	}
};

}

#endif /* _GARBAGE_THREADS_H_ */
