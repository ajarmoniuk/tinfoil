/*
 * configurationhelper.h
 *
 *  Created on: 22-04-2012
 *      Author: ajarmoniuk
 */

#ifndef CONFIGURATIONHELPER_H_
#define CONFIGURATIONHELPER_H_

#include "configuration.h"

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/lexical_cast.hpp>
#include <stdexcept>

using namespace std;
using namespace boost::filesystem;
namespace po = boost::program_options;

namespace tinfoil {

class unrecognized_section_error : public runtime_error {
public:
	unrecognized_section_error(const string & section_name);
};

class parsing_error : public runtime_error {
public:
	parsing_error(const string & tok, unsigned line_nr) :
		runtime_error(string("Parsing error: invalid token: ")
				.append(tok)
				.append(" in line ")
				.append(boost::lexical_cast<string>(line_nr))) {}
};

/**
 * Helper class for parsing the configuration file.
 */
class configuration_parser {
public:

	enum configuration_section {
		NONE, GENERAL, ALLOWED_CLIENTS, SAFE_SITES, UNSAFE_SITES
	};

	/**
	 * Creates a configuration_parser object and binds it
	 * to a given configuration object.
	 */
	configuration_parser(configuration &);

	/**
	 * Parses a given config file and stores the result in
	 * the bound configuration object.
	 */
	void parse_file(const string & file_name);

	/**
	 * A convenience method for parsing a given file name
	 * and storing the results into a given configuration object.
	 */
	static void load_from_file(configuration & config, const string & file_name);

protected:
	configuration & config;
	configuration_section section;
	unsigned line_num;

	void handle_assignment(const string &, const string &);
	void handle_token(const string &);
	static configuration_section get_section(const string &) throw (unrecognized_section_error);
};

}

#endif /* CONFIGURATIONHELPER_H_ */
