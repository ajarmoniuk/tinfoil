/*
 * httprequest.cpp
 *
 *  Created on: 24-05-2012
 *      Author: ajarmoniuk
 */

#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>

#include "httprequest.h"

namespace tinfoil {

void http_request::parse_first_line(string const & line) {
	static const boost::regex line_regex("(\\S+)\\s(\\S+)\\s(\\S+)");

	boost::smatch matches;
	if (boost::regex_match(line, matches, line_regex)) {
		set_method(matches[1]);
		set_uri(matches[2]);
		set_version(matches[3]);
	}
}

string http_request::get_protocol() {
	if (uri_protocol.empty())
		initialise_uri_parts();
	return uri_protocol;
}

string http_request::get_peer_name() {
	if (uri_peer_name.empty())
		initialise_uri_parts();
	return uri_peer_name;
}

string http_request::get_peer_port() {
	if (uri_peer_port.empty())
		initialise_uri_parts();
	return uri_peer_port;
}

void http_request::initialise_uri_parts() {
	static const boost::regex uri_regex("([^:]+)://([^/^:]+)(:[^/]+)?.*");

	boost::smatch matches;
	if (boost::regex_match(uri, matches, uri_regex, boost::match_extra)) {
		uri_protocol = matches[1];
		uri_peer_name = matches[2];
		uri_peer_port = matches[3];
		if (uri_peer_port.empty() == false) {
			uri_peer_port = uri_peer_port.substr(1);
		}
	}

}

} /* namespace tinfoil */
