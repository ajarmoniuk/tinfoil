/*
 * daemon.cpp
 *
 *  Created on: 22-04-2012
 *      Author: ajarmoniuk
 */

#include "daemon.h"
#include "configurationhelper.h"
#include "socket.h"
#include "httpserver.h"
#include "logger.h"
#include "garbage_threads.h"
#include <vector>

using namespace std;

namespace tinfoil {

daemon::daemon(configuration & config) : config(config) {
	logger::set_level(config.get_log_level());
	logger::set_output_file(config.get_log_file_name());
	logger::set_verbose(config.is_verbose());
}

daemon::daemon(const string & config_file) {
	configuration_parser::load_from_file(config, config_file);
	logger::set_level(config.get_log_level());
	logger::set_output_file(config.get_log_file_name());
	logger::set_verbose(config.is_verbose());
}

daemon::~daemon() {
	// TODO Auto-generated destructor stub
}

void daemon::start() {
	socket s(config.get_listener_addr(), config.get_listener_port());
	logger(info) << "Waiting for connections on " << config.get_listener_addr() << ":" << config.get_listener_port() << "...";

	while (true) {
		http_server * server = 0;
        garbage_threads<http_server> gt;

		try {
			socket cs = s.accept();
			logger(info) << "Accepted connection ";
		    server = new http_server(config, cs);
            gt.run(server);
		}
		catch (runtime_error const & e) {
			logger(error) << e.what();
		}
		catch (...) {
			logger(error) << "Unknown error: " << strerror(errno);
		}
	}
}

}
