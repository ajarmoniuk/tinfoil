/*
 * httpserver.h
 *
 *  Created on: 01-05-2012
 *      Author: ajarmoniuk
 */

#ifndef HTTPSERVER_H_
#define HTTPSERVER_H_

#include <boost/thread.hpp>
#include <boost/unordered_map.hpp>
#include <istream>

#include "socket.h"
#include "httprequest.h"
#include "configuration.h"

namespace tinfoil {

/**
 * This is a class handling connections from client.
 */
class http_server {
protected:
	tinfoil::socket socket;

	boost::thread thread;

	configuration config;

	/**
	 * Internal method used by boost::thread execution.
	 */
	void run();

	/**
	 * Reads a http_request from the socket and returns an initialised http_request object.
	 */
	http_request read_http_request() const throw (network_error);

	/**
	 * Reads http headers from source connection.
	 */
	 void read_http_headers(boost::unordered_map<string, string> &) const throw(network_error);

	/**
	 * Sends a given http_request to the outgoing socket.
	 */
	void send_http_request(tinfoil::socket &, http_request &) throw (network_error);

	/**
	 * Relays data from remote to local peers.
	 */
	void relay_connection(tinfoil::socket &);

	/**
	 * Reads file from given stream and sends to local peer.
	 */
	void relay_connection(istream &);

	/**
	 * Returns true if a given peer name appears on the safe peer list.
	 */
	bool whitelisted(const string &);

	/**
	 * Returns file name of the content for a given peer name.
	 */
	string const & content_for_peer(const string &);

	/**
	 * Test constructor
	 */
	http_server(configuration const config) : config(config) {}

	/**
	 * Sends error page to client.
	 * @param err HTTP result code
	 */
	void send_error_page(int err);

public:
	/**
	 * Creates a http_server instance, assigns the given socket object with the http_server object.
	 */
	http_server(configuration const config, tinfoil::socket & socket);

	/**
	 * Destroys the http_server instance.
	 */
	virtual ~http_server();

	/**
	 * Reads the input from the client and processes it.
	 */
	void start();

	/**
	 * Waits for the server's thread to finish.
	 */
	void join();

};

} /* namespace tinfoil */
#endif /* HTTPSERVER_H_ */
