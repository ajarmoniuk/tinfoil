/*
 * httpserver.cpp
 *
 *  Created on: 01-05-2012
 *      Author: ajarmoniuk
 */

#include "httpserver.h"
#include "socket.h"
#include "logger.h"
#include "errors.h"

#include <iostream>
#include <istream>
#include <fstream>
#include <stdio.h>
#include <string.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>

#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>
#include <boost/regex.hpp>

/* The IP length is set to 48, since IPv6 can be that long */
#define IP_LENGTH		48
#define HOSTNAME_LENGTH		1024
#define BUF_SIZE			16000

#define MAXLINE (1024 * 4)

using namespace std;

namespace tinfoil {

http_server::http_server(configuration const config, tinfoil::socket & socket) :
		socket(socket), config(config) {
	this->socket.acquire(socket);
}

http_server::~http_server() {
}

void http_server::start() {
	thread = boost::thread(&http_server::run, this);
}

void http_server::run() {
	try {
		logger(info) << "Processing connection from " << socket.get_peer_name();
		//for (int i = 0; i < 3; ++i) { cout << socket.read_line() << endl; }

		http_request incoming_request = read_http_request();
		string port = incoming_request.get_peer_port();
		if (port.empty()) {
			port = "80";
		}

		logger(debug) << "Method:\t" << incoming_request.get_method();
		logger(debug) << "URI:\t" << incoming_request.get_uri();
		logger(debug) << "Version:\t" << incoming_request.get_version();

		if (incoming_request.get_protocol() != "http") {
			return;
		}

		if (whitelisted(incoming_request.get_peer_name())) {
			tinfoil::socket outgoing_socket;
			http_request outgoing_request(incoming_request);
			outgoing_request.set_version("HTTP/1.0");
			outgoing_socket.connect(incoming_request.get_peer_name(), port);
			send_http_request(outgoing_socket, outgoing_request);
			relay_connection(outgoing_socket);
		} else {
			string const & content_name = content_for_peer(
					incoming_request.get_peer_name());
			ifstream file(content_name.c_str());
			if (file.good()) {
				relay_connection(file);
			} else {
				logger(error) << "Error reading from " << content_name << ", "
						<< strerror(errno);
			}
		}
		logger(info) << "Closing connection to "
				<< incoming_request.get_peer_name() << ":" << port;
	} catch (network_error const & e) {
		switch (e.get_err_no()) {
		case 2:
			send_error_page(404);
			break;
		}
		logger(error) << e.what();
	} catch (runtime_error const & e) {
		logger(error) << e.what();
	} catch (...) {
		logger(error) << "Unknown error: " << strerror(errno);
	}
	socket.reset();
}

void http_server::join() {
	thread.join();
}

void http_server::read_http_headers(
		unordered_map<string, string> & headers) const throw (network_error) {
	static const boost::regex re_header("([^: ]+):\\s*(.*)");
	static const boost::regex re_header_cont("\\s*(.*)");

	headers.clear();
	smatch matches;
	for (string line, key = "", lkey, value;
			(line = socket.read_line()).empty() == false;) {
		logger(debug) << "Received line: \"" << line << "\"";
		if (regex_match(line, matches, re_header)) {
			key = matches[1];
			value = matches[2];
			lkey = to_lower_copy(key);
			if (lkey != "connection" && lkey != "proxy-connection"
					&& lkey != "te" && lkey != "trailers"
					&& lkey != "upgrade") {
				headers[key] = value;
			}
		} else if (key != "" && regex_match(line, matches, re_header)) {
			value += " " + matches[2];
			if (lkey != "connection" && lkey != "proxy-connection"
					&& lkey != "te" && lkey != "trailers"
					&& lkey != "upgrade") {
				headers[key] = value;
			}
		} else {
			logger(warning) << "Error parsing header line: \"" << line << "\"";
		}
	}
}

http_request http_server::read_http_request() const throw (network_error) {
	string line = socket.read_line();
	http_request request = http_request(line);
	read_http_headers(request.get_headers());

	return request;
}

void http_server::send_http_request(tinfoil::socket & outgoing_socket,
		http_request &request) throw (network_error) {
	string line = request.get_method() + " " + request.get_uri() + " "
			+ request.get_version();
	logger(debug) << "Sending line: \"" << line << "\"";
	outgoing_socket.send_line(line);
	//outgoing_socket.send_line("");

	unordered_map<string, string> & headers = request.get_headers();
	headers["Connection"] = "close";
	for (unordered_map<string, string>::const_iterator i = headers.begin();
			i != headers.end(); ++i) {
		string key = (*i).first, value = (*i).second;
		string header = key + ": " + value;
		outgoing_socket.send_line(header);
	}
	outgoing_socket.send_line("");
}

void http_server::relay_connection(tinfoil::socket & outgoing_socket) {
	logger(info) << "Relaying started...";
	char buf[BUF_SIZE];
	long num_read;
	do {
		try {
			num_read = outgoing_socket.read_buffer(static_cast<char *>(buf),
					BUF_SIZE);
			socket.send_buffer(static_cast<char *>(buf), num_read);
			logger(debug) << "Relayed " << num_read << " bytes.";
		} catch (network_error const & e) {
			logger(warning) << e.what();
		}
	} while (num_read > 0);
}

void http_server::relay_connection(istream & stream) {
	logger(info) << "Sending file contents started...";
	char buf[BUF_SIZE];

	while (stream.eof() == false) {
		try {
			stream.read(buf, BUF_SIZE);
			int count = stream.gcount();
			logger(debug) << count << " bytes read.";
			socket.send_buffer(buf, count);
		} catch (network_error const & e) {
			logger(warning) << e.what();
		}
	}
}

void http_server::send_error_page(int err) {
	try {
		socket.send_line(error_page(err));
	} catch (runtime_error const & e) {
		logger(error) << "Error while sending error page to client: "
				<< e.what();
	} catch (...) {
		logger(error) << "Unknown error while sending error page to client.";
	}
}

bool http_server::whitelisted(const string & peer_name) {
	for (unordered_set<string>::const_iterator i =
			config.get_safe_sites().begin(); i != config.get_safe_sites().end();
			++i) {
		boost::regex re(*i);
		if (boost::regex_match(peer_name, re)) {
			logger(debug) << peer_name << " matches " << *i;
			return true;
		}
	}
	return false;
}

string const & http_server::content_for_peer(const string & peer_name) {
	for (unordered_map<string, string>::const_iterator i =
			config.get_unsafe_sites().begin();
			i != config.get_unsafe_sites().end(); ++i) {
		boost::regex re((*i).first);
		if (boost::regex_match(peer_name, re)) {
			logger(debug) << peer_name << " matches " << (*i).first;
			return (*i).second;
		}
	}
	return config.get_default_content();
}

} /* namespace tinfoil */
