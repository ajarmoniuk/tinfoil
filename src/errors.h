#ifndef _ERRORS_H_
#define _ERRORS_H_

#include <utility>
#include <map>
#include <string>

namespace tinfoil {

typedef std::pair<std::string,std::string> err_warning;
typedef std::map<int,err_warning> err_warning_lst;

class error_page
{
  private:
    static std::string code;
    static err_warning_lst lst;
    int which;

    std::string head() const;
    std::string top() const;
    std::string bottom() const;
    std::string body() const;

  public:
    error_page(int);
    virtual ~error_page();
    static void init();
    static void set(int,std::string,std::string);

    std::string to_string();

    operator std::string();
    friend std::ostream & operator<<(std::ostream &out, const error_page &e);
};
}
#endif /* _ERRORS_H_ */
