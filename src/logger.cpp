#include "logger.h"

//namespace tinfoil {

using namespace std;

log_level logger::minimumLevel = warning;
std::string logger::file_name = "/tmp/tinfoil.log";
char const * logger::log_level_str[] = { "debug", "info", "warning", "error",
		"none" };
boost::mutex logger::mutex;
bool logger::verbose = false;

logger::~logger() {
	log(level, buffer.str());
}

logger & logger::operator <<(char const * str) {
	buffer << str;
	return *this;
}

logger & logger::operator <<(string const & str) {
	buffer << str;
	return *this;
}

logger & logger::operator <<(int i) {
	buffer << i;
	return *this;
}

void logger::log(log_level l, std::string const &s) {
	mutex.lock();
	if (l != none && l >= minimumLevel) {
		//zapisuje do pliku
		ofstream outfile;
		try {
			outfile.open(file_name.c_str(), std::ios_base::app);
			boost::posix_time::ptime now =
					boost::posix_time::second_clock::local_time();
			outfile << boost::posix_time::to_simple_string(now).c_str()
					<< " :: (" << log_level_str[l] << ") :: " << s << "\n";
			if (verbose) {
				cout << s << endl;
			}
		} catch (...) {
			std::cerr << "Error writing to log file: " << strerror(errno)
					<< std::endl << std::flush;
		}
	}
	mutex.unlock();
}

void logger::set_output_file(std::string const &s) {
	mutex.lock();
	file_name = s;
	mutex.unlock();
}

log_level logger::get_level() {
	mutex.lock();
	log_level l = minimumLevel;
	mutex.unlock();
	return l;
}
void logger::set_level(log_level l) {
	mutex.lock();
	minimumLevel = l;
	mutex.unlock();
}

void logger::set_verbose(bool verbose) {
	mutex.lock();
	logger::verbose = verbose;
	mutex.unlock();
}

bool logger::is_verbose() {
	return verbose;
}

char const * const logger::log_level_to_string(log_level level) {
	return log_level_str[level];
}

log_level logger::string_to_log_level(string const & str) {
	int l;
	for (l = 0; static_cast<log_level>(l) != none; ++l) {
		if (str == log_level_str[l]) {
			break;
		}
	}
	return static_cast<log_level>(l);
}

//}
