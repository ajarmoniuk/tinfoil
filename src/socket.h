/*
 * socket.h
 *
 *  Created on: 01-05-2012
 *      Author: ajarmoniuk
 */

#ifndef SOCKET_H_
#define SOCKET_H_

#include <string>
#include <stdexcept>
#include <sys/socket.h>
#include <sys/types.h>

using namespace std;

namespace tinfoil {

/**
 * Generic network error.
 */
class network_error : public runtime_error {
protected:
	int err_no;

public:
	network_error() throw();

	network_error(string const & msg) throw();

	int get_err_no() const;

	string get_err_str() const;
};

/**
 * Invalid family used to create a socket or retrieve information on a socket.
 */
class invalid_family_error : public runtime_error {
public:
	invalid_family_error(int family);
};

class socket {
public:
	/** file descriptor associated with the socket */
	int fd;

	/**
	 * Create and bind a socket using the given address, port and family.
	 */
	socket(const string& address, const string& port, int family = AF_INET) throw (network_error);

	/**
	 * Copy constructor. Steals the fd from the source freeing it from the need to close the fd.
	 */
	socket(const socket&);

	/**
	 * Binds to any (local) address and port.
	 */
	socket() throw (network_error);

	/**
	 * Destructor, frees the fd.
	 */
	virtual ~socket();

	/**
	 * Blocks current thread and starts listening for incoming connections.
	 * Returns a socket object initialized with socket fd of the client connection.
	 */

	socket accept();
	/**
	 * Acquires ownership of another socket's fd. The called socket will now be responsible
	 * for managing the file descriptor. The socket passed as argument ceases to be responsible
	 * for the resource.
	 */
	void acquire(socket& s);
	/**
	 * Ceases to manage the resource fd.
	 */
	void disown();
	/**
	 * Closes the file descriptor associated with the socket.
	 */
	void close();
	/**
	 * Tests if the file descriptor is set and closes it.
	 */
	void reset();
	/**
	 * Connects to a remote peer.
	 */
	void connect(string const & peer_name, string const & peer_port) throw (network_error);

	/**
	 * Reads a single line from socket.
	 * Assumes all lines end with CRLF (not just LF, not just CR, and not LFCR).
	 */
	string read_line() const throw (network_error);

	/**
	 * Reads into a binary buffer. Returns number of bytes read.
	 */
	ssize_t read_buffer(void *, size_t) const throw (network_error);

	/**
	 * Sends a binary buffer. Returns number of bytes sent.
	 */
	void send_buffer(char const *, size_t) const throw (network_error);

	/**
	 * Sends a single line through socket.
	 * Appends a CRLF after the end of line.
	 * Returns reference to self.
	 */
	socket const & send_line(string const &) const throw (network_error);

	string const get_local_name() const;

	string const get_peer_name() const;

	string const get_local_ip() const throw (invalid_family_error);

	string const get_peer_ip() const throw (invalid_family_error);

	static string const sockaddr_to_name(sockaddr const &);

	static string const sockaddr_to_ip(sockaddr const &) throw (invalid_family_error);

private:
};

} /* namespace tinfoil */
#endif /* SOCKET_H_ */
