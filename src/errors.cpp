#include "errors.h"

namespace tinfoil {

std::string error_page::code = "UTF-8";
err_warning_lst error_page::lst;

std::string error_page::head() const
{
    std::string ret;
    ret.append("<html><head>"
               "<title>");
    ret.append(lst[which].first);
    ret.append("</title>"
               "<META http-equiv=\"content-type\" content=\"text/html; charset=");
    ret.append(code);
    ret.append("\">"
               "</head>");
    return ret;
}

std::string error_page::top() const
{
    std::string ret;
    ret.append("<html>");
    return ret;
}

std::string error_page::bottom() const
{
    std::string ret;
    ret.append("</html>");
    return ret;
}

std::string error_page::body() const
{
    std::string ret;
    ret.append("<h1>");
    ret.append(lst[which].first);
    ret.append("</h1><p>");
    ret.append(lst[which].second);
    ret.append("</p>");
    return ret;
}

error_page::error_page(int w) : which(w)
{

}

void error_page::init()
{
    // ERRORS 3XX
    set(300, "300 Multiple Choices", "Indicates multiple options for the resource that the client may follow. It, for instance, could be used to present different format options for video, list files with different extensions, or word sense disambiguation.");
    set(301, "301 Moved Permanently", "This and all future requests should be directed to the given URI.");
    set(302, "302 Found", "This is an example of industry practice contradicting the standard. The HTTP/1.0 specification (RFC 1945) required the client to perform a temporary redirect (the original describing phrase was \"Moved Temporarily\"), but popular browsers implemented 302 with the functionality of a 303 See Other. Therefore, HTTP/1.1 added status codes 303 and 307 to distinguish between the two behaviours. However, some Web applications and frameworks use the 302 status code as if it were the 303.[citation needed]");
    set(303, "303 See Other (since HTTP/1.1)", "The response to the request can be found under another URI using a GET method. When received in response to a POST (or PUT/DELETE), it should be assumed that the server has received the data and the redirect should be issued with a separate GET message.");
    set(304, "304 Not Modified", "Indicates the resource has not been modified since last requested. Typically, the HTTP client provides a header like the If-Modified-Since header to provide a time against which to compare. Using this saves bandwidth and reprocessing on both the server and client, as only the header data must be sent and received in comparison to the entirety of the page being re-processed by the server, then sent again using more bandwidth of the server and client.");
    set(305, "305 Use Proxy (since HTTP/1.1)", "Many HTTP clients (such as Mozilla and Internet Explorer) do not correctly handle responses with this status code, primarily for security reasons.");
    set(306, "306 Switch Proxy", "No longer used. Originally meant \"Subsequent requests should use the specified proxy.\"");
    set(307, "307 Temporary Redirect (since HTTP/1.1)", "In this case, the request should be repeated with another URI; however, future requests can still use the original URI. In contrast to 302, the request method should not be changed when reissuing the original request. For instance, a POST request must be repeated using another POST request.");
    set(308, "308 Permanent Redirect (experimental Internet-Draft)", "The request, and all future requests should be repeated using another URI. 307 and 308 (as proposed) parallel the behaviours of 302 and 301, but do not require the HTTP method to change. So, for example, submitting a form to a permanently redirected resource may continue smoothly.");
    // ERRORS 4XX
    set(400, "400 Bad Request", "The request cannot be fulfilled due to bad syntax.");
    set(401, "401 Unauthorized", "Similar to 403 Forbidden, but specifically for use when authentication is possible but has failed or not yet been provided. The response must include a WWW-Authenticate header field containing a challenge applicable to the requested resource. See Basic access authentication and Digest access authentication.");
    set(402, "402 Payment Required", "Reserved for future use. The original intention was that this code might be used as part of some form of digital cash or micropayment scheme, but that has not happened, and this code is not usually used. As an example of its use, however, Apple's MobileMe service generates a 402 error (\"httpStatusCode:402\" in the Mac OS X Console log) if the MobileMe account is delinquent.[citation needed]");
    set(403, "403 Forbidden", "The request was a legal request, but the server is refusing to respond to it. Unlike a 401 Unauthorized response, authenticating will make no difference.");
    set(404, "404 Not Found", "The requested resource could not be found but may be available again in the future. Subsequent requests by the client are permissible.");
    set(405, "405 Method Not Allowed", "A request was made of a resource using a request method not supported by that resource; for example, using GET on a form which requires data to be presented via POST, or using PUT on a read-only resource.");
    set(406, "406 Not Acceptable", "The requested resource is only capable of generating content not acceptable according to the Accept headers sent in the request.");
    set(407, "407 Proxy Authentication Required" ,"The client must first authenticate itself with the proxy.");
    set(408, "408 Request Timeout", "The server timed out waiting for the request. According to W3 HTTP specifications: \"The client did not produce a request within the time that the server was prepared to wait. The client MAY repeat the request without modifications at any later time.\"");
    set(409, "409 Conflict", "Indicates that the request could not be processed because of conflict in the request, such as an edit conflict.");
    set(410, "410 Gone", "Indicates that the resource requested is no longer available and will not be available again. This should be used when a resource has been intentionally removed and the resource should be purged. Upon receiving a 410 status code, the client should not request the resource again in the future. Clients such as search engines should remove the resource from their indices. Most use cases do not require clients and search engines to purge the resource, and a \"404 Not Found\" may be used instead.");
    set(411, "Length Required", "The request did not specify the length of its content, which is required by the requested resource.");
    set(412, "412 Precondition Failed", "The server does not meet one of the preconditions that the requester put on the request.");
    set(413, "413 Request Entity Too Large", "The request is larger than the server is willing or able to process.");
    set(414, "414 Request-URI Too Long", "The URI provided was too long for the server to process.");
    set(415, "415 Unsupported Media Type", "The request entity has a media type which the server or resource does not support. For example, the client uploads an image as image/svg+xml, but the server requires that images use a different format.");
    set(416, "416 Requested Range Not Satisfiable", "The client has asked for a portion of the file, but the server cannot supply that portion. For example, if the client asked for a part of the file that lies beyond the end of the file.");
    set(417, "417 Expectation Failed", "The server cannot meet the requirements of the Expect request-header field.");
    set(418, "418 I'm a teapot (RFC 2324)", "This code was defined in 1998 as one of the traditional IETF April Fools' jokes, in RFC 2324, Hyper Text Coffee Pot Control Protocol, and is not expected to be implemented by actual HTTP servers. However, known implementations do exist.");
    set(420, "420 Enhance Your Calm (Twitter)", "Returned by the Twitter Search and Trends API when the client is being rate limited. Likely a reference to this number's association with marijuana. Other services may wish to implement the 429 Too Many Requests response code instead. The phrase \"Enhance Your Calm\" is a reference to Demolition Man (film). In the film, Sylvester Stallone's character John Spartan is a hot-head in a generally more subdued future, and is regularly told to \"Enhance your calm\" rather than a more common phrase like \"calm down\".");
    set(422, "422 Unprocessable Entity (WebDAV; RFC 4918)", "The request was well-formed but was unable to be followed due to semantic errors.");
    set(423, "423 Locked (WebDAV; RFC 4918)", "The resource that is being accessed is locked.");
    set(424, "424 Method Failure (WebDAV)", "Indicates the method was not executed on a particular resource within its scope because some part of the method's execution failed causing the entire method to be aborted.");
    set(425, "425 Unordered Collection (Internet draft)", "Defined in drafts of \"WebDAV Advanced Collections Protocol\" but not present in \"Web Distributed Authoring and Versioning (WebDAV) Ordered Collections Protocol\".");
    set(426, "426 Upgrade Required (RFC 2817)", "The client should switch to a different protocol such as TLS/1.0.");
    set(428, "428 Precondition Required (RFC 6585)", "The origin server requires the request to be conditional. Intended to prevent \"the 'lost update' problem, where a client GETs a resource's state, modifies it, and PUTs it back to the server, when meanwhile a third party has modified the state on the server, leading to a conflict.\"");
    set(429, "429 Too Many Requests (RFC 6585)", "The user has sent too many requests in a given amount of time. Intended for use with rate limiting schemes.");
    set(431, "431 Request Header Fields Too Large (RFC 6585)", "The server is unwilling to process the request because either an individual header field, or all the header fields collectively, are too large.");
    set(443, "443 uses an invalid certificate.", "");
    set(444, "444 No Response (Nginx)", "An Nginx HTTP server extension. The server returns no information to the client and closes the connection (useful as a deterrent for malware).");
    set(449, "449 Retry With (Microsoft)", "A Microsoft extension. The request should be retried after performing the appropriate action.");
    set(450, "450 Blocked by Windows Parental Controls (Microsoft)", "A Microsoft extension. This error is given when Windows Parental Controls are turned on and are blocking access to the given webpage.");
    set(499, "499 Client Closed Request (Nginx)", "An Nginx HTTP server extension. This code is introduced to log the case when the connection is closed by client while HTTP server is processing its request, making server unable to send the HTTP header back.");
    // ERRORS 5XX
    set(500, "500 Internal Server Error", "A generic error message, given when no more specific message is suitable.");
    set(501, "501 Not Implemented", "The server either does not recognise the request method, or it lacks the ability to fulfill the request.");
    set(502, "502 Bad Gateway", "The server was acting as a gateway or proxy and received an invalid response from the upstream server.");
    set(503, "503 Service Unavailable", "The server is currently unavailable (because it is overloaded or down for maintenance). Generally, this is a temporary state.");
    set(504, "504 Gateway Timeout", "The server was acting as a gateway or proxy and did not receive a timely response from the upstream server.");
    set(504, "505 HTTP Version Not Supported", "The server does not support the HTTP protocol version used in the request.");
    set(506, "506 Variant Also Negotiates (RFC 2295)", "Transparent content negotiation for the request results in a circular reference.");
    set(507, "507 Insufficient Storage (WebDAV; RFC 4918)", "The server is unable to store the representation needed to complete the request.");
    set(508, "508 Loop Detected (WebDAV; RFC 5842)", "The server detected an infinite loop while processing the request (sent in lieu of 208).");
    set(509, "509 Bandwidth Limit Exceeded (Apache bw/limited extension)", "This status code, while used by many servers, is not specified in any RFCs.");
    set(510, "510 Not Extended (RFC 2774)", "Further extensions to the request are required for the server to fulfill it.");
    set(511, "511 Network Authentication Required (RFC 6585)", "The client needs to authenticate to gain network access. Intended for use by intercepting proxies used to control access to the network (e.g. \"captive portals\" used to require agreement to Terms of Service before granting full Internet access via a Wi-Fi hotspot).");
    set(598, "598 Network read timeout error (Unknown)", "This status code is not specified in any RFCs, but is used by Microsoft Corp. HTTP proxies to signal a network read timeout behind the proxy to a client in front of the proxy.");
    set(599, "599 Network connect timeout error (Unknown)", "This status code is not specified in any RFCs, but is used by Microsoft Corp. HTTP proxies to signal a network connect timeout behind the proxy to a client in front of the proxy.");
}

error_page::~error_page()
{

}

void error_page::set(int n,std::string head,std::string body)
{
    lst[n] = err_warning(head,body);
}

std::ostream & operator<<(std::ostream &out, const error_page &e)
{
    out << e;
    return out;
}

std::string error_page::to_string() {
	return head() + top() + body() + bottom();
}

error_page::operator std::string() {
	return to_string();
}

}
