/*
 * httprequest.h
 *
 *  Created on: 24-05-2012
 *      Author: ajarmoniuk
 */

#ifndef HTTPREQUEST_H_
#define HTTPREQUEST_H_

#include <string>
#include <boost/unordered_map.hpp>

namespace tinfoil {

using namespace std;
using namespace boost;

/**
 * Represents a single HTTP Request.
 */
class http_request {
public:
private:
	string method;
	string uri;
	string version;
	unordered_map<string, string> headers;

	// uri parts cache
	string uri_protocol;
	string uri_peer_name;
	string uri_peer_port;

public:
	/** Constructs an empty, uninitialised http_request object. */
	http_request() {}

	/**
	 * Constructs a http_request object initialised with the first line.
	 * Any headers must be filled separately.
	 *
	 * @param first_line a string containing the first line of the request
	 */
	http_request(string const & first_line) {
		parse_first_line(first_line);
	}

	/**
	 * Returns HTTP method.
	 */
	string const & get_method() const {
		return method;
	}

	/** Sets HTTP method accepting string as input */
	void set_method(string const & method) {
		this->method = method;
	}

	/**
	 * Returns the resource URI.
	 */
	string const & get_uri() const {
		return uri;
	}

	/**
	 * Sets the resource URI.
	 */
	void set_uri(string const & uri) {
		this->uri = uri;

		// clear uri parts cache
		uri_protocol = uri_peer_name = uri_peer_port = "";
	}

	/**
	 * Returns HTTP version associated with the request.
	 */
	string const & get_version() const {
		return version;
	}

	/**
	 * Sets HTTP version parameter associated with the request.
	 */
	void set_version(string const & version) {
		this->version = version;
	}

	/**
	 * Returns a reference to the list of headers associated with the request.
	 */
	unordered_map<string, string> & get_headers() {
		return headers;
	}

	/** Returns protocol part of URI */
	string get_protocol();

	/** Returns peer name part of URI */
	string get_peer_name();

	/** Returns peer port part of URI or 80 if not present */
	string get_peer_port();

protected:
	/**
	 * Parses a given line and sets the object's method, uri and version fields.
	 */
	void parse_first_line(string const &);

private:
	/**
	 * Initialises the uri parts "cache" based on uri.
	 */
	void initialise_uri_parts();

};

} /* namespace tinfoil */
#endif /* HTTPREQUEST_H_ */
