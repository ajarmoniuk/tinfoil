/*
 * configuration.h
 *
 *  Created on: 21-04-2012
 *      Author: ajarmoniuk
 */

#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_

#include "logger.h"

#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>

using namespace std;
using namespace boost;

namespace tinfoil {

/**
 * Models configuration options of the server.
 */
class configuration {
private:
	bool verbose;

	string log_file_name;

	log_level loglevel;

	string owner;

	string owner_group;

	string listener_addr;

	string listener_port;

	unordered_set<string> allowed_clients;

	unordered_set<string> safe_sites;

	unordered_map<string, string> unsafe_sites;

	string default_content;

public:
	configuration();

	virtual ~configuration();

	log_level get_log_level() const {
		return loglevel;
	}

	void set_log_level(log_level level) {
		loglevel = level;
	}

	const string & get_log_file_name() const {
		return log_file_name;
	}

	void set_log_file_name(string const & log_file_name) {
		this->log_file_name = log_file_name;
	}

	const string & get_listener_addr() const {
		return listener_addr;
	}

	void set_listener_addr(const string & listenerAddr) {
		listener_addr = listenerAddr;
	}

	unordered_set<string> & get_allowed_clients() {
		return allowed_clients;
	}

	const string & get_default_content() const {
		return default_content;
	}

	void set_default_content(const string & defaultContent) {
		default_content = defaultContent;
	}

	const string & get_listener_port() const {
		return listener_port;
	}

	void set_listener_port(const string & listenerPort) {
		listener_port = listenerPort;
	}

	const string & get_owner() const {
		return owner;
	}

	void set_owner(const string & owner) {
		this->owner = owner;
	}

	const string & get_owner_group() const {
		return owner_group;
	}

	void set_owner_group(const string & owner_group) {
		this->owner_group = owner_group;
	}

	unordered_set<string> & get_safe_sites() {
		return safe_sites;
	}

	unordered_map<string, string> & get_unsafe_sites() {
		return unsafe_sites;
	}

	bool is_verbose() const {
		return verbose;
	}

	void set_verbose(bool verbose) {
		this->verbose = verbose;
	}

	friend class configuration_parser;
};

} /* namespace tinfoil */

#endif /* CONFIGURATION_H_ */
