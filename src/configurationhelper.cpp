/*
 * configurationhelper.cpp
 *
 *  Created on: 22-04-2012
 *      Author: ajarmoniuk
 */

#include "configurationhelper.h"
#include "logger.h"

#include <iostream>
#include <fstream>
#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

namespace tinfoil {

configuration_parser::configuration_parser(configuration & c) :
		config(c), section(NONE) {
}

unrecognized_section_error::unrecognized_section_error(
		const string & section_name) :
		runtime_error(string("Unrecognized section: ") + section_name) {
}

configuration_parser::configuration_section configuration_parser::get_section(
		const string & section_name) throw (unrecognized_section_error) {
	if (section_name == "general") {
		return GENERAL;
	}
	if (section_name == "allowed clients") {
		return ALLOWED_CLIENTS;
	}
	if (section_name == "safe sites") {
		return SAFE_SITES;
	}
	if (section_name == "unsafe sites") {
		return UNSAFE_SITES;
	}
	throw unrecognized_section_error(section_name);
}

void configuration_parser::handle_assignment(const string & attr,
		const string & val) {
	switch (section) {
	case GENERAL:
		if (attr == "listener_port") {
			config.listener_port = val;
		} else if (attr == "log_file_name") {
			config.log_file_name = val;
		} else if (attr == "verbose") {
			if (val == "true") {
				config.verbose = true;
			} else if (val == "false") {
				config.verbose = false;
			} else {
				throw runtime_error("invalid value: " + val);
			}
		}
		else if (attr == "log_level") {
			config.loglevel = logger::string_to_log_level(val);
		}
		else if (attr == "listener_addr") {
			config.listener_addr = val;
		}
		else {
			throw parsing_error(attr, line_num);
		}
		break;
	case NONE:
	case ALLOWED_CLIENTS:
	case SAFE_SITES:
		throw parsing_error(attr, line_num);
	case UNSAFE_SITES:
		config.get_unsafe_sites()[attr] = val;
		break;
	}
}

void configuration_parser::handle_token(const string & tok) {
	switch (section) {
	case GENERAL:
	case NONE:
		throw parsing_error(tok, line_num);
	case SAFE_SITES:
		config.get_safe_sites().emplace(tok);
		break;
	case ALLOWED_CLIENTS:
		config.get_allowed_clients().emplace(tok);
		break;
	case UNSAFE_SITES:
		config.get_unsafe_sites()[tok] = config.get_default_content();
		break;
	}
}

void configuration_parser::parse_file(const string & file_name) {
	ifstream ifs(file_name.c_str(), ios::in);
	if (!ifs) {
		throw runtime_error("Cannot load config from file " + file_name);
	}

	static const boost::regex header("\\s*\\[([^\\]]+)\\]\\s*");
	static const boost::regex assignment("\\s*(\\S+)\\s*=\\s*(\\S+)\\s*");
	static const boost::regex token("\\s*(\\S+)\\s*");
	string line;
	boost::smatch matches;

	for (line_num = 1; ifs.good(); line_num++) {
		getline(ifs, line);

		// strip comments
		size_t comment_pos = line.find('#');
		if (comment_pos != string::npos) {
			line = line.substr(0, comment_pos);
		}

		if (regex_match(line, matches, header)) {
			section = get_section(matches[1]);
		} else if (regex_match(line, matches, assignment)) {
			handle_assignment(string(matches[1]), string(matches[2]));
		} else if (regex_match(line, matches, token)) {
			handle_token(string(matches[1]));
		}
	}

}

void configuration_parser::load_from_file(configuration & config,
		const string & file_name) {
	configuration_parser parser(config);
	parser.parse_file(file_name);
}

}
