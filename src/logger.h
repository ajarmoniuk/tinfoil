#ifndef _LOGGER_H_
#define _LOGGER_H_

#include <string>
#include <fstream>
#include <sstream>
#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

using namespace std;

//namespace tinfoil {

enum log_level {
	debug = 0, info = 1, warning = 2, error = 3, none = 4
};

class logger {
protected:
	log_level level;
	stringstream buffer;

	static logger * instance;
	static boost::mutex mutex;
	static std::string file_name;
	static char const * log_level_str[];
	static log_level minimumLevel;
	static bool verbose;

	static void log(log_level, std::string const &);

public:
	/**
	 * "Functional object"
	 * Constructs a local logger object initialised with the given level.
	 */
	logger(log_level level) : level(level) {}

	/**
	 * "Functional desctructor"
	 * Flushes the buffer to the log.
	 */
	virtual ~logger();

	/** Appends a string. */
	logger & operator << (string const &);

	/** Appends a C-string. */
	logger & operator << (char const *);

	/** Appends an int. */
	logger & operator << (int);

	static void set_output_file(std::string const &);

	static log_level get_level();

	static void set_level(log_level);

	static void set_verbose(bool verbose);

	static bool is_verbose();

	/** Converts log_level to char const * */
	static char const * const log_level_to_string(log_level);

	/** Converts string to log_level */
	static log_level string_to_log_level(string const &);
};

//}

#endif /* _LOGGER_H_ */
