/*
 * socket.cpp
 *
 *  Created on: 01-05-2012
 *      Author: ajarmoniuk
 */

#include "socket.h"
#include "logger.h"

#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <arpa/inet.h>
#include <boost/format.hpp>

namespace tinfoil {

#define SEGMENT_LEN (512)

network_error::network_error() throw () :
		runtime_error(strerror(errno)), err_no(errno) {
}

network_error::network_error(string const & msg) throw () :
		runtime_error((boost::format("%s (%d: %d)") % msg % errno % strerror(errno)).str()), err_no(
				errno) {
}

int network_error::get_err_no() const {
	return err_no;
}

string network_error::get_err_str() const {
	return strerror(err_no);
}

invalid_family_error::invalid_family_error(int family) :
		runtime_error((boost::format("Invalid family: %d") % family).str()) {
}

/**
 * Create and bind a socket using the given address, port and family.
 */
socket::socket(const string & address, const string & port, int af_family)
		throw (network_error) :
		fd(0) {
	addrinfo hints, *result, *rp;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = af_family;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	if (getaddrinfo(address.c_str(), port.c_str(), &hints, &result) != 0) {
		throw network_error(
				(boost::format("Error while trying to getaddrinfo of %s:%s")
						% address % port).str());
	}

	for (rp = result; rp != 0; rp = rp->ai_next) {
		const int on = 1;

		fd = ::socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
		if (fd == -1) {
			continue;
		}

		setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof on);
		if (bind(fd, rp->ai_addr, rp->ai_addrlen) == 0) {
			break; // success
		}

		::close(fd);
	}
	freeaddrinfo(result);
	if (rp == 0) {
		throw network_error(
				(boost::format("Error while trying to bind to %s:%s") % address
						% port).str());
	}
}

socket::socket(const socket & src) :
		fd(src.fd) {
}

socket::socket() throw (network_error) :
		fd(0) {
	fd = ::socket(AF_INET, SOCK_STREAM, 0);
	if (fd == -1) {
		throw network_error("Error while trying to create sockets");
	}
}

socket::~socket() {
	reset();
}

socket socket::accept() {
	if (fd != 0 && listen(fd, 1024) < 0) {
		throw network_error();
	}

	socket s(*this);
	sockaddr client_addr;
	socklen_t ca_len = sizeof(struct sockaddr);

	// We need the client address information so that we can refuse the connection
	// if the client is blacklisted.
	s.fd = ::accept(fd, &client_addr, &ca_len);
	logger(info) << "received connection from " << sockaddr_to_name(client_addr)
			<< " (" + sockaddr_to_ip(client_addr) << ")";

	return s;
}

void socket::connect(string const & peer_name, string const & peer_port)
		throw (network_error) {
	addrinfo hints, *result, *rp;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	if (getaddrinfo(peer_name.c_str(), peer_port.c_str(), &hints, &result)
			!= 0) {
		throw network_error(
				(boost::format("Error while trying to getaddrinfo of %s:%s")
						% peer_name % peer_port).str());
	}

	for (rp = result; rp != 0; rp = rp->ai_next) {
		if (::connect(fd, rp->ai_addr, rp->ai_addrlen) == 0) {
			break;
		}
	}

	freeaddrinfo(result);
	if (rp == 0) {
		throw network_error(
				(boost::format("Error while trying to connect to %s:%s")
						% peer_name % peer_port).str());
	}
}

void socket::acquire(socket & s) {
	disown();
	std::swap(fd, s.fd);
}

void socket::disown() {
	fd = 0;
}

void socket::close() {
	logger(debug) << "closing fd " << fd;
	if (fd != 0 && ::close(fd) == -1) {
		throw network_error();
	}
}

void socket::reset() {
	if (fd != 0) {
		close();
		fd = 0;
	}
}

string const socket::get_local_name() const {
	sockaddr sa;
	socklen_t sa_len = sizeof(sockaddr);
	getsockname(fd, &sa, &sa_len);
	return sockaddr_to_name(sa);
}

string const socket::get_peer_name() const {
	sockaddr sa;
	socklen_t sa_len = sizeof(sockaddr);
	getpeername(fd, &sa, &sa_len);
	return sockaddr_to_name(sa);
}

string const socket::get_local_ip() const throw (invalid_family_error) {
	sockaddr sa;
	socklen_t sa_len = sizeof(sockaddr);
	getsockname(fd, &sa, &sa_len);
	return sockaddr_to_ip(sa);
}

string const socket::get_peer_ip() const throw (invalid_family_error) {
	sockaddr sa;
	socklen_t sa_len = sizeof(sockaddr);
	getpeername(fd, &sa, &sa_len);
	return sockaddr_to_ip(sa);
}

string const socket::sockaddr_to_name(sockaddr const & sa) {
	char name_buf[NI_MAXHOST];
	getnameinfo(&sa, sizeof(struct sockaddr), name_buf, NI_MAXHOST, 0, 0, 0);
	return string(name_buf);
}

string const socket::sockaddr_to_ip(sockaddr const & sa)
		throw (invalid_family_error) {
	switch (sa.sa_family) {
	case AF_INET: {
		sockaddr_in const * sa_in = reinterpret_cast<sockaddr_in const *>(&sa);
		char buf[INET_ADDRSTRLEN];

		inet_ntop(AF_INET, &sa_in->sin_addr, buf, INET_ADDRSTRLEN);
		return string(buf);
	}
	case AF_INET6: {
		sockaddr_in6 const *sa_in6 = reinterpret_cast<sockaddr_in6 const *>(&sa);
		char buf[INET6_ADDRSTRLEN];

		inet_ntop(AF_INET6, &sa_in6->sin6_addr, buf, INET6_ADDRSTRLEN);
		return string(buf);
	}
	}
	throw invalid_family_error(sa.sa_family);
}

string socket::read_line() const throw (network_error) {
	char buffer[SEGMENT_LEN];
	string result;
	ssize_t chars_read = 0;

	for (bool eof_reached = false; !eof_reached;
			recv(fd, buffer, chars_read, 0)) {
		int recv_result = recv(fd, buffer, SEGMENT_LEN, MSG_PEEK);
		if (recv_result == 0) {
			return result;
		}
		if (recv_result == -1) {
			throw network_error("Cannot read from socket");
		}
		char * ptr = static_cast<char*>(memchr(buffer, '\n', SEGMENT_LEN));
		if (ptr != 0) {
			eof_reached = true;
			chars_read = ptr - buffer + 1;
			result.append(buffer, chars_read - 2); // assuming all lines end with \r\n
		} else {
			chars_read = SEGMENT_LEN;
			result.append(buffer);
		}
	}

	return result;
}

ssize_t socket::read_buffer(void * buffer, size_t len) const
		throw (network_error) {
	ssize_t chars_read = recv(fd, buffer, len, 0);
	if (chars_read == -1) {
		throw network_error("Cannot read from socket");
	}
	return chars_read;
}

void socket::send_buffer(char const * bytes, size_t len) const
		throw (network_error) {
	do {
		ssize_t num_bytes = send(fd, bytes, len, MSG_NOSIGNAL);
		if (num_bytes >= 0) {
			len -= num_bytes;
			bytes += num_bytes;
		} else {
			throw network_error("Error while trying to send to peer");
		}
	} while (len > 0);
}

socket const & socket::send_line(string const & line) const
		throw (network_error) {
	static char const * const crlf = "\r\n";
	send_buffer(line.c_str(), line.size());
	send_buffer(crlf, 2);

	return *this;
}

} /* namespace tinfoil */
