/*
 * test_configuration.cpp
 *
 *  Created on: 22-04-2012
 *      Author: ajarmoniuk
 */

#include "logger.h"
#include <boost/test/unit_test.hpp>

using namespace boost;

BOOST_AUTO_TEST_CASE( logger_tests ) {
	BOOST_CHECK_EQUAL(debug, logger::string_to_log_level("debug"));
}
