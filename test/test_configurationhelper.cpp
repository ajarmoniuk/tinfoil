/*
 * test_configuration.cpp
 *
 *  Created on: 22-04-2012
 *      Author: ajarmoniuk
 */

#include "configuration.h"
#include "configurationhelper.h"

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>

using namespace boost;
using namespace boost::filesystem;
using namespace tinfoil;

BOOST_AUTO_TEST_CASE( set_loading_file )
{
    configuration config;
    configuration_parser::load_from_file(config, "config.cfg");

    BOOST_CHECK_EQUAL("8080", config.get_listener_port());
    // testing default value:
    BOOST_CHECK_EQUAL(true, config.is_verbose());

    BOOST_CHECK_EQUAL(2, config.get_allowed_clients().size());

    BOOST_CHECK_EQUAL("localhost", config.get_listener_addr());

    BOOST_CHECK_EQUAL(2, config.get_safe_sites().size());

	BOOST_CHECK_EQUAL("../onet.html", config.get_unsafe_sites()["www.onet.pl"]);
}
