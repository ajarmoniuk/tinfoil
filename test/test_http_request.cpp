/*
 * test_socket.cpp
 *
 *  Created on: 01-05-2012
 *      Author: ajarmoniuk
 */

#include <boost/test/unit_test.hpp>

#include "httprequest.h"

using namespace tinfoil;
using namespace boost;

BOOST_AUTO_TEST_CASE( http_request_parse_line ) {
	http_request request("GET http://www.w3.org/pub/WWW/TheProject.html HTTP/1.1");
	BOOST_CHECK_EQUAL("GET", request.get_method());
	BOOST_CHECK_EQUAL("http://www.w3.org/pub/WWW/TheProject.html", request.get_uri());
	BOOST_CHECK_EQUAL("HTTP/1.1", request.get_version());
}

BOOST_AUTO_TEST_CASE( http_request_uri_tools_no_port ) {
	http_request request("GET http://www.w3.org/pub/WWW/TheProject.html HTTP/1.1");
	BOOST_CHECK_EQUAL("http", request.get_protocol());
	BOOST_CHECK_EQUAL("www.w3.org", request.get_peer_name());
	BOOST_CHECK_EQUAL("", request.get_peer_port());
}

BOOST_AUTO_TEST_CASE( http_request_uri_tools_with_port ) {
	http_request request("GET http://www.w3.org:8080/pub/WWW/TheProject.html HTTP/1.1");
	BOOST_CHECK_EQUAL("http", request.get_protocol());
	BOOST_CHECK_EQUAL("www.w3.org", request.get_peer_name());
	BOOST_CHECK_EQUAL("8080", request.get_peer_port());
}
