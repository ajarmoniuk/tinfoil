/*
 * test_configuration.cpp
 *
 *  Created on: 22-04-2012
 *      Author: ajarmoniuk
 */

#include "configuration.h"
#include <boost/test/unit_test.hpp>
#include <boost/unordered_set.hpp>
#include <boost/foreach.hpp>
#include <boost/regex.hpp>

using namespace boost;
using namespace tinfoil;

BOOST_AUTO_TEST_CASE( set_allowed_clients )
{
    configuration config;

    unordered_set<string> & allowed_clients = config.get_allowed_clients();
    allowed_clients.emplace("mion");
    BOOST_CHECK_EQUAL( 1, allowed_clients.size() );
    BOOST_CHECK( allowed_clients.find("mion") != allowed_clients.end() );

    allowed_clients.erase("mion");
    BOOST_CHECK_EQUAL( 0, allowed_clients.size() );
    BOOST_CHECK( allowed_clients.find("mion") == allowed_clients.end() );

    allowed_clients.emplace("mion");
    allowed_clients.emplace("wizzar");
    allowed_clients.emplace("mion");
    BOOST_CHECK_EQUAL( 2, allowed_clients.size() );

    BOOST_CHECK_EQUAL( "wizzar", * allowed_clients.find("wizzar") );

    config.set_listener_addr("localhost");
    BOOST_CHECK_EQUAL("localhost", config.get_listener_addr());

	config.get_safe_sites().emplace("www.+");
	config.get_safe_sites().emplace("www.wp.pl");
	string peer_name = "www.onet.pl";
	BOOST_CHECK_EQUAL(2, config.get_safe_sites().size());
}
