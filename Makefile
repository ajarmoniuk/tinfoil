TINFOIL_OBJS = $(TINFOIL_APP) $(TINFOIL_DEP)

CXX = clang
# CXX = g++

TINFOIL_APP = bin/tinfoil.o
TINFOIL_DEP = bin/daemon.o bin/configuration.o bin/configurationhelper.o bin/socket.o bin/httpserver.o bin/httprequest.o bin/logger.o bin/errors.o
TINFOIL_LIBS =  -lboost_system -lboost_filesystem -lboost_program_options -lboost_regex -lboost_thread -lboost_date_time
TINFOIL_H = src/daemon.h src/configuration.h src/configurationhelper.h src/socket.h src/httpserver.h src/httprequest.h src/logger.h src/errors.h

TESTRUNNER_LIBS = $(TINFOIL_LIBS) -lboost_unit_test_framework
TESTRUNNER_OBJS = test-bin/runner.o test-bin/test_configuration.o test-bin/test_configurationhelper.o test-bin/test_http_request.o test-bin/test_logger.o

CONFIG_FILE	= config.cfg

all:	tinfoil testrunner

clean:	clean-tinfoil clean-test

clean-tinfoil:
	rm bin/*

clean-test:
	rm test-bin/*

tinfoil:	$(TINFOIL_OBJS)
	$(CXX)  -o bin/tinfoil $(TINFOIL_OBJS) $(TINFOIL_LIBS)
	@echo	> bin/$(CONFIG_FILE) "# THIS FILE IS OVERWRITTEN EACH TIME A TEST BUILD IS MADE "
	@cat	>> bin/$(CONFIG_FILE) src/$(CONFIG_FILE)
	@cp		src/*.html	bin/

testrunner:	$(TESTRUNNER_OBJS) tinfoil
	$(CXX)  -o test-bin/testrunner $(TINFOIL_DEP) $(TESTRUNNER_OBJS) $(TESTRUNNER_LIBS)
	@echo	> test-bin/$(CONFIG_FILE) "# THIS FILE IS OVERWRITTEN EACH TIME A TEST BUILD IS MADE "
	@cat	>> test-bin/$(CONFIG_FILE) test/$(CONFIG_FILE)
	@cp		test/*.html	test-bin/

bin/%.o: src/%.cpp $(TINFOIL_H)
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	$(CXX) -DBOOST_REGEX_DYN_LINK -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ''
	
test-bin/%.o: test/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	$(CXX) -O0 -g3 -Wall -DBOOST_TEST_DYN_LINK -c -fmessage-length=0 -MMD -MP -I "./src" -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ''
